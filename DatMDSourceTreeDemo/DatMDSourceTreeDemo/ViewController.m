//
//  ViewController.m
//  DatMDSourceTreeDemo
//
//  Created by Mai Duc Dat on 3/24/15.
//  Copyright (c) 2015 Mai Duc Dat. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self doSomething];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)doSomething
{
    NSLog(@"do something");
}
@end
