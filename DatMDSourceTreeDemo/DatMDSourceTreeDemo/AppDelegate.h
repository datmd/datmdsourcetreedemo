//
//  AppDelegate.h
//  DatMDSourceTreeDemo
//
//  Created by Mai Duc Dat on 3/24/15.
//  Copyright (c) 2015 Mai Duc Dat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

